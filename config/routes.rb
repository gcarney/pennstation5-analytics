
PennStation::Backend::Engine.routes.draw do

  page = begin
           PennStation::Backend::Page.find_by_ptype('Analytics')
         rescue Exception => e
           puts "Tables not created yet. Run db:migrate unless that's what you are doing."
           nil
         end

  if page.present?
    scope module: 'analytics' do
      resources :analytics, path: page.strip_home_path
    end
  end
end
