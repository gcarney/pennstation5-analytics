module PennStation
  module Analytics
    class Setting < ActiveRecord::Base
      self.table_name = 'penn_station_analytics_settings'

      OAUTH_CLIENT_ID = "309649842280-svjb0s5v36iajuklrvmcr42o65cetbtg.apps.googleusercontent.com"
      OAUTH_CLIENT_SECRET = "PAaUIRQu9R3zVELblDSnmlcS"
    end
  end
end

    # Analytics Module information
    #
    # Module is registered at google cloud console for access only to Google Analytics API.
    #
    # GEMS
    # oauth2, https://github.com/intridea/oauth2, provides oauth2 authorization layer
    # legato, https://github.com/tpitale/legato, provides google analytics api v3 layer
    #
    #
    # OAUTH2
    # We use the Google Oauth2 "Installed Apps" flow. Normally used for desktop applications, but recommended
    # for content management system plugins.
    #
    # Basic flow:
    # 1. generate authorization url based on app's client id
    # 2. user visits authorization url and is prompted to authorize access for our app
    # 3. when user accepts, google generates authorization code
    # 4. user copies authorization code and submits it to app
    # 5. app uses authorization code to obtain access token and refresh token
    # 6. app uses access token to retrieve user analytics data. when access token expires, app uses
    #    refresh token to request new access token
    #
    # See https://developers.google.com/analytics/devguides/reporting/core/v3/gdataAuthorization#common_oauth
    #
    #
    # GOOGLE ANALYTICS API
    # See https://developers.google.com/analytics/devguides/reporting/core/v3/
    # and https://developers.google.com/analytics/devguides/config/mgmt/v3/
    #
    #
    # HIERARCHY OF ACCOUNTS, PROPERTIES, AND VIEWS (FORMERLY KNOWN AS PROFILES)
    # owner
    #   analytics account
    #   ...
    #   analytics account
    #     property <==> domain1.com <==> UA-XXXXXXXX-01
    #     ...
    #     property <==> domain5.com <==> UA-XXXXXXXX-05
    #       view1 = usually all analytics stats
    #       view2 = customized analytics stats
    #       view3 = customized analytics stats
    #       ...
    #
    # See https://support.google.com/analytics/answer/1009618?hl=en&ref_topic=1102143&rd=1
    #

    # CCTS PS Analytics Module app info, from google cloud console https://cloud.google.com/console‎
