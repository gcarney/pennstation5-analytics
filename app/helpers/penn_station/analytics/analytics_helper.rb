module PennStation
  module Analytics
    module AnalyticsHelper

      def analytics_authorization_url
        #client = OAuth2::Client.new(PennStation::AnalyticsSetting::OAUTH_CLIENT_ID,
        #                            PennStation::AnalyticsSetting::OAUTH_CLIENT_SECRET, {
        client = ::OAuth2::Client.new(Setting::OAUTH_CLIENT_ID,
                                      Setting::OAUTH_CLIENT_SECRET, {
          :authorize_url => 'https://accounts.google.com/o/oauth2/auth',
          :token_url => 'https://accounts.google.com/o/oauth2/token'
        })

        client.auth_code.authorize_url({:scope => 'https://www.googleapis.com/auth/analytics.readonly',
          :redirect_uri => 'urn:ietf:wg:oauth:2.0:oob'
        })
      end

      # for use on public part of site
      def google_analytics_embed_code_from_db
        analytics_setting = Setting.first

        if analytics_setting.nil? or analytics_setting.tracking_code.blank?
          ""
        else
          analytics_setting.tracking_code.html_safe
        end
      end

    end
  end
end

