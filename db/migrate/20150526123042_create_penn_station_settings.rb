class CreatePennStationSettings < ActiveRecord::Migration
  def change
    create_table :penn_station_analytics_settings do |t|
      t.string :tracking_id
      t.string :view_id
      t.string :tracking_code
      t.string :access_token
      t.string :refresh_token
      t.string :expires_at

      t.timestamps null: false
    end
  end
end
