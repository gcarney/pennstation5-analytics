$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "penn_station/analytics/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "penn_station_analytics"
  s.version     = PennStation::Analytics::VERSION
  s.authors     = ["warren vosper"]
  s.email       = ["straydogsw@gmail.com"]
  s.homepage    = "http://cctsbaltimore.org"
  s.summary     = "Analytics features of PennStation."
  s.description = "Analytics features of PennStation."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 4.2.4"
  s.add_dependency "legato", '=0.4.0'
  s.add_dependency "oauth2", '=1.0.0'

  s.add_development_dependency "sqlite3"
  s.add_dependency "penn_station_core"
end
