module PennStation
  module Analytics
    class Engine < ::Rails::Engine
      isolate_namespace PennStation

      initializer :append_migrations do |app|
        unless app.root.to_s.match(root.to_s)
          config.paths["db/migrate"].expanded.each do |p|
            app.config.paths["db/migrate"] << p
          end
        end
      end

      initializer :assets do |config|
        Rails.application.config.assets.precompile += %w{ penn_station/analytics/application.css }
        Rails.application.config.assets.precompile += %w{ penn_station/analytics/application.js }

        Rails.application.config.assets.precompile += %w{ penn_station/analytics/penn_station.css }
        Rails.application.config.assets.precompile += %w{ penn_station/analytics/penn_station.js }

        Rails.application.config.assets.paths << root.join("app", "assets", "images", "penn_station", "search", "frontend")
        Rails.application.config.assets.paths << root.join("app", "assets", "images", "penn_station", "search", "backend")
      end
    end
  end
end
